﻿using Atlassian.Jira;
using Atlassian.Jira.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class TestJiraFactory : IJiraFactory
    {
        private const int DefaultTotalIssues = 500;
        private string[] _jiraStatuses = new string[2] { "Open", "In Progress" };
        private string[] _jiraPriorities = new string[2] { "High", "Low" };
        private string[] _jiraTypes = new string[2] { "Bug", "Feature" };
        private string[] _jiraResolutions = new string[2] { "Fixed", "Duplicate" };
        private readonly int _totalIssues;

        public TestJiraFactory(int? totalIssues = null)
        {
            this._totalIssues = totalIssues ?? DefaultTotalIssues;
        }

        public Jira Create(string url, string username, string password)
        {
            username = username ?? "username";
            password = password ?? "password";
            var client = new TestJiraClient(this._totalIssues);
            var jira = Jira.CreateRestClient(client, new JiraCredentials(username, password), GetJiraCache());
            client.Jira = jira;
            return jira;
        }

        private JiraCache GetJiraCache()
        {
            var cache = new JiraCache();
            cache.Statuses.AddIfMIssing(Enumerable.Range(0, 2).Select(i =>
                new IssueStatus(new RemoteNamedObject()
                {
                    id = i.ToString(),
                    name = _jiraStatuses[i]
                })));

            cache.Priorities.AddIfMIssing(Enumerable.Range(0, 2).Select(i =>
                new IssuePriority(new RemoteNamedObject()
                {
                    id = i.ToString(),
                    name = _jiraPriorities[i]
                })));

            cache.Resolutions.AddIfMIssing(Enumerable.Range(0, 2).Select(i =>
                new IssueResolution(new RemoteNamedObject()
                {
                    id = i.ToString(),
                    name = _jiraResolutions[i]
                })));

            var issueTyes = Enumerable.Range(0, 2).Select(i =>
                new IssueType(new RemoteIssueType()
                {
                    id = i.ToString(),
                    name = _jiraTypes[i]
                }));

            cache.IssueTypes.AddIfMIssing(new JiraEntityDictionary<IssueType>("[ALL_PROJECTS]", issueTyes));

            return cache;
        }
    }
}
