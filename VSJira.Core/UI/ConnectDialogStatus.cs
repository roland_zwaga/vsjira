﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public enum ConnectDialogStatus
    {
        Busy,
        Idle,
        ValidationError,
        ConnectionError
    }
}
