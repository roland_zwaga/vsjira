﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VSJira.Core.UI
{
    public class JiraIssuesPagerViewModel : PagerViewModelBase<Issue>
    {
        public class ColumnHeaderChangeEventArgs : EventArgs
        {
            public string Field { get; set; }
            public ListSortDirection? SortDirection { get; set; }
        }

        private string _sortField;
        private ListSortDirection? _sortDirection;
        private string _jql;

        public event EventHandler<ColumnHeaderChangeEventArgs> ColumnHeaderChanged;
        public ObservableCollection<JiraIssueViewModel> Issues { get; private set; }

        public JiraIssuesPagerViewModel(VSJiraServices services)
            : base(services)
        {
            this.Issues = new ObservableCollection<JiraIssueViewModel>();
        }

        protected override void OnOperationCancelled()
        {
            this.Issues.Clear();
            base.OnOperationCancelled();
        }

        protected override void OnResettingItems()
        {
            // Clear the sorting UI
            OnColumnHeaderChanged(this._sortField, null);
            this._sortField = null;
            this._sortDirection = null;
            base.OnResettingItems();
        }

        protected override void OnLoadingItems()
        {
            this.Issues.Clear();
            base.OnLoadingItems();
        }

        protected override async Task<IPagedQueryResult<Issue>> GetItemsAsync(int itemsPerPage, int startAt, CancellationToken token)
        {
            var jql = PrepareJql(this._jql);
            var issues = await this.Jira.RestClient.GetIssuesFromJqlAsync(jql, itemsPerPage, startAt, token);

            foreach (var issue in issues)
            {
                this.Issues.Add(new JiraIssueViewModel(issue, this.Services));
            }

            return issues as IPagedQueryResult<Issue>;
        }

        public Task ResetItemsAsync(string jql, int maxIssuesPerRequest)
        {
            this._jql = jql;
            return this.ResetItemsAsync(maxIssuesPerRequest);
        }

        public async Task SortIssuesAsync(string field)
        {
            if (!field.Equals(this._sortField, StringComparison.OrdinalIgnoreCase))
            {
                this.OnColumnHeaderChanged(this._sortField, null);
                this._sortDirection = ListSortDirection.Ascending;
                this._sortField = field;
            }
            else
            {
                this._sortDirection = this._sortDirection != ListSortDirection.Ascending ?
                    ListSortDirection.Ascending : ListSortDirection.Descending;
            }

            this._currentItemIndex = 0;
            this.OnColumnHeaderChanged(this._sortField, this._sortDirection);

            await this.LoadItemsAsync();
        }

        private string PrepareJql(string jql)
        {
            if (this._sortDirection == null)
            {
                return jql;
            }

            var orderByIndex = jql.IndexOf("Order by", StringComparison.OrdinalIgnoreCase);

            if (orderByIndex > 0)
            {
                jql = jql.Substring(0, orderByIndex);
            }

            return String.Format("{0} Order by {1} {2}",
                jql,
                this._sortField,
                this._sortDirection == ListSortDirection.Ascending ? "asc" : "desc");
        }

        private void OnColumnHeaderChanged(string field, ListSortDirection? sortDirection)
        {
            var args = new ColumnHeaderChangeEventArgs()
            {
                Field = field,
                SortDirection = sortDirection
            };

            var handler = this.ColumnHeaderChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}
