﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraIssuesUserControlViewModel : JiraBackedViewModel
    {
        private JiraFilter _selectedFilter;
        private CancellationTokenSource _filtersCancellationSource = new CancellationTokenSource();

        public JiraIssuesPagerViewModel IssuePager { get; private set; }
        public ObservableCollection<JiraFilter> Filters { get; private set; }
        public DelegateCommand<object> OpenConnectDialogCommand { get; private set; }
        public DelegateCommand<object> RefreshCommand { get; private set; }
        public DelegateCommand<object> ShowOptionsPageCommand { get; private set; }

        public JiraIssuesUserControlViewModel(VSJiraServices services)
            : base(services)
        {
            this.IssuePager = new JiraIssuesPagerViewModel(services);
            this.Filters = new ObservableCollection<JiraFilter>();
            this.OpenConnectDialogCommand = new DelegateCommand<object>(async (args) => await this.OpenConnectDialog());
            this.RefreshCommand = new DelegateCommand<object>(async (args) => await this.Refresh());
            this.ShowOptionsPageCommand = new DelegateCommand<object>((args) => this.ShowOptionsPage());
        }

        public JiraFilter SelectedFilter
        {
            get { return _selectedFilter; }
            set
            {
                _selectedFilter = value;
                OnPropertyChanged("SelectedFilter");

                if (this.SelectedFilter != null)
                {
                    var maxIssuesPerRequest = this.Services.VisualStudioServices.GetConfigurationOptions().MaxIssuesPerRequest;
                    this.IssuePager.ResetItemsAsync(this.SelectedFilter.Jql, maxIssuesPerRequest);
                }
            }
        }

        private Task<IEnumerable<JiraFilter>> GetFiltersAsync(CancellationToken token)
        {
            return this.Jira.RestClient.GetFavouriteFiltersAsync(token);
        }

        private async Task OpenConnectDialog()
        {
            this._filtersCancellationSource.Cancel();

            var vm = new ConnectDialogViewModel(this.Services.JiraFactory, this.Services);
            this.Services.WindowFactory.ShowDialog(vm);

            if (vm.Jira != null)
            {
                this.Jira = vm.Jira;
                this.IssuePager.Jira = vm.Jira;

                await LoadFiltersComboBoxAsync();
            }
        }

        private async Task LoadFiltersComboBoxAsync(JiraFilter selectedFilter = null)
        {
            this._filtersCancellationSource = new CancellationTokenSource();
            this.Filters.Clear();

            IEnumerable<JiraFilter> filters = null;

            try
            {
                filters = (await GetFiltersAsync(this._filtersCancellationSource.Token)).ToArray();
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("There was an error retrieving the filters from server. Details: " + ex.Message);
            }

            if (filters != null)
            {
                foreach (var filter in filters)
                {
                    this.Filters.Add(filter);
                }

                if (selectedFilter != null)
                {
                    // Re-select the previously selected filter
                    this.SelectedFilter = filters.FirstOrDefault(f => f.Id == selectedFilter.Id);
                }
                else
                {
                    // Automatically select the first filter.
                    this.SelectedFilter = filters.FirstOrDefault();
                }
            }
        }

        private void ShowOptionsPage()
        {
            this.Services.VisualStudioServices.ShowOptionsPage();
        }

        private async Task Refresh()
        {
            if (this.IsConnected)
            {
                this.IssuePager.CancelOperation();
                this._filtersCancellationSource.Cancel();
                await LoadFiltersComboBoxAsync(this.SelectedFilter);
            }
        }
    }
}