﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class TestPagedIssueQueryResult : IPagedQueryResult<Issue>
    {
        public int ItemsPerPage { get; set; }
        public int StartAt { get; set; }
        public int TotalItems { get; set; }
        public IEnumerable<Issue> Issues { get; set; }

        public IEnumerator<Issue> GetEnumerator()
        {
            return this.Issues.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.Issues.GetEnumerator();
        }
    }
}
