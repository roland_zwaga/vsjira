# Change History

** Version 2.1.0 (12/05/2015) **

* Add ability to view and edit comments on issues.

** Version 2.0.0 (12/02/2015) **

* Add ability to switch between Light and Dark themes. Fixes https://bitbucket.org/farmas/vsjira/issues/6.

** Version 1.3.0 (11/26/2015) **

* Add ability to update JIRA issue fields within the tool window.
- Use current culture to format dates. Fixes https://bitbucket.org/farmas/vsjira/issues/7. 

** Version 1.2.0 (11/25/2015) **

* Add a multi-instance VisualStudio tool window to show read-only information of an issue.